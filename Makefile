### add your documentation files here

DOCFILES=README CHANGELOG

### no user servicable parts below

ROCK=$(shell basename src/*.rockspec .rockspec)

all:
	@echo targets are pack and html
	@echo to build the module, enter src and make there.

# make html documentation from text (markdown) documentation

html: $(DOCFILES:%=html/%.html)

html/%.html: src/doc/%
	multimarkdown $< >> $@

# build archive for luarocks

pack:
	cd src && make clean
	cd src && luarocks lint $(ROCK).rockspec
	mkdir $(ROCK)
	cp -r src/* $(ROCK)
	tar czf $(ROCK).tar.gz $(ROCK)
	rm -rf $(ROCK)
